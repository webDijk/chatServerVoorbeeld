﻿using System;
using System.Threading;
using Fleck;
using System.Collections.Generic;
using System.IO;


namespace chatServer
{	

	/* 
	 * - Server class -
	 * in this class are 
	 * the functions to
	 * run the server
	 * ----------------	 
	 */
	public class Server
	{
		//the string that contains the server url
		private string url { get; set; }
		private bool running = false;
		private Debugger logger = new Debugger();
		//list with connected sockets
		public List<Fleck.IWebSocketConnection> sockets {get; set;}
		//the server socket
		private WebSocketServer socket;
		//constructor with given url
		public Server(string url_){
			url = url_;
		}

		//function to get the status of the server
		public bool getStatus(){
			return running;
		}

		//function to start the server
		public void start(){
			running = true;
			//update the server
			update ();

			//creating a new thread
			Thread t = new Thread(new ThreadStart( delegate {

				//start the fleck socket
				socket.Start(socket => {

					//if a socket try's to connect
					socket.OnOpen = () =>{
						//log that someone tried to connect
						logger.log(socket.ConnectionInfo.ClientIpAddress + " has tried to connect");
						//if it isn't blacklisted
						if(validate(socket.ConnectionInfo.ClientIpAddress)){
							//log that the client has connected
							logger.log(socket.ConnectionInfo.ClientIpAddress + " has connected", "succes");		
							//notify the client that it is accepted
							socket.Send("accepted");
							//add the client to the list with connected clients
							sockets.Add(socket);
							//notify every connected client that the other client is connected
							sockets.ForEach(s => s.Send(socket.ConnectionInfo.ClientIpAddress + " has joined"));						
						}
						else{
							//log that the client is blacklisted
							logger.log(socket.ConnectionInfo.ClientIpAddress + " is blacklisted and is rejected", "warning");

							//reject the socket
							socket.Close();
						}
					};
					//if the server receives a message
					socket.OnMessage = (message) =>{
						//send every connected client the message							
						sockets.ForEach(s => s.Send(socket.ConnectionInfo.ClientIpAddress + ": "  + message));
					};
					//if someone disconnect
					socket.OnClose = () => {
						//remove the client from the list with connected clients
						sockets.RemoveAll(s => s.ConnectionInfo == socket.ConnectionInfo);
					};
						 
				});

			}));
			//start the thread
			t.Start ();
		}

		public void stop(){
			socket.Dispose ();
		}

		//function to update the server variables
		public void update(){
			socket = new WebSocketServer (url);
			sockets = new List<IWebSocketConnection>();
			//log that the server location has changed
			logger.log ("the server location has updated to " + url);
		}

		//function to validate the given ip
		public bool validate(string clientIp){
			//for each blocked ip in the blacklist
			foreach(var ip in blackList()){
				logger.log (ip + " - " + clientIp, "debug");
				//if the client's ip equals the blocked ip part
				if(clientIp.Equals(ip)){ 
					//log that the client is blacklisted
					logger.log("client ip equals blacklist ip: " + ip, "debug");
					return false;
				}
			}
			//the client isn't blacklisted
			return true;
		}

		//function that retrieves the blacklist
		public List<string> blackList(){
			//create a new list
			List<string> ipList = new List<string> ();

			//sets the blacklist location
			string blackListLocation = Environment.CurrentDirectory + "/blacklist.txt";
			//retreives the blacklist text
			string blackListString = File.ReadAllText(blackListLocation);
			//creates an array containing every ip in the blacklist
			string[] ips = blackListString.Split ('\n');

			//adds all the ip's that are longer than 1 character to the list
			foreach (var ip in ips) {
				if (ip.Length > 1) {
					ipList.Add (ip);
					//just some debug log
					logger.log (ip + " is added to the blacklist", "debug");

				}
			}
			//returns the iplist
			return ipList;
		}

	}
}

