﻿using System;
using Fleck;
using System.Threading;

namespace chatServer
{
	class MainClass
	{
		//variable that indicates if it is debugging or not
		public static bool debug = false;

		//main function of the program
		public static void Main (string[] args)
		{
			//create a new server	
			Server s = new Server ("ws://0.0.0.0:8888");

			//start the new server
			s.start ();
			bool running = true;
			//don't stop the server while it is running
			while (running) {
				running = s.getStatus();
				Thread.Sleep (1000);
			}
		}
	}
}
