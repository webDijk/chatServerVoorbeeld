﻿using System;

namespace chatServer
{
	public class Debugger
	{
		/*
		 * - debugger class -
		 * this class handles 
		 * the debug code 
		 * ------------------
		 */

		//this is the log function it needs a message and a log level
		public void log(string message, string level ="info"){
			//sets the terminal color according to the log level
			switch (level) {

			case "error":
				Console.ForegroundColor = ConsoleColor.Red;
				break;

			case "warning":
				Console.ForegroundColor = ConsoleColor.DarkYellow;
				break;

			case "succes":
				Console.ForegroundColor = ConsoleColor.Green;
				break;

			case "debug":
				Console.ForegroundColor = ConsoleColor.Cyan;
				break;

			default:
				Console.ForegroundColor = ConsoleColor.White;
				break;
			}
			//if the log level is debug and the program isn't debugging
			//then this code stops the logging
			if (level == "debug" && !MainClass.debug) {
				Console.ForegroundColor = ConsoleColor.White;
				return;
			}

			//log the message
			Console.WriteLine ("[{0}] {1}", level, message);
			//resets the color to white
			Console.ForegroundColor = ConsoleColor.White;
		}
	}
}

