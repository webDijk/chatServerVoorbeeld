<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="css/main.css">
  </head>
  <body>
    <div class="container">
    <div id="chatBox">


    </div>
    <div class="controls">
      <input type="text" id="messageBox" placeholder="message">
      <button type="button" onclick="send()" id="randomBtn" name="button">send</button>
    </div>
  </div>

    <script src="js/main.js" charset="utf-8"></script>
    </body>
</html>
